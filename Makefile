#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "Licence").
# You may not use this file except in compliance with the Licence.
#
# You can obtain a copy of the licence at
# cddl/RiscOS/Sources/HAL/Titanium/LICENCE.
# See the Licence for the specific language governing permissions
# and limitations under the Licence.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the Licence file. If applicable, add the
# following below this CDDL HEADER, with the fields enclosed by
# brackets "[]" replaced with your own identifying information:
# Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
#
# Copyright 2014 Elesar Ltd.  All rights reserved.
# Use is subject to license terms.
#
# Makefile for Titanium HAL
#

COMPONENT  = Titanium HAL
TARGET     = Titanium
OBJS       = Top Entries Init Timers IIC Interrupts KbdScan Muxing \
             NVMemory USB Utils Video Counter SDHCI EtherNIC Miscellaneous \
             RTC PCI PCIProbing PMIC SATA UART GPIO Audio CPUClk SDMA

include HAL

CFLAGS    += -APCS 3/32bit/nofp/noswst -DPCI_SWITCHES=0
ASFLAGS   += -APCS 3/nofp/noswst -PD "Vayu SETL {FALSE}"

# Dynamic dependencies:
